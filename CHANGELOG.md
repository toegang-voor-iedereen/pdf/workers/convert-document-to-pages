## [1.13.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-pages/compare/1.13.0...1.13.1) (2025-01-20)


### Bug Fixes

* update base worker ([a65d03b](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-pages/commit/a65d03bff09b32cac7f4f2dee1fa353655f27cdb))

# [1.13.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-pages/compare/1.12.0...1.13.0) (2024-12-16)


### Features

* **deps:** kimiworker 4.4.0 ([2fa8bf4](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-pages/commit/2fa8bf4892bb707ea5a18231049c08e80e5ee377))

# [1.12.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-pages/compare/1.11.0...1.12.0) (2024-12-11)


### Features

* **deps:** kimiworker v4.1.0 - using remote file storage and new debugfile name ([b863893](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-pages/commit/b863893f423c5c816398a3074998cba6ec264f8c))

# [1.11.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-pages/compare/1.10.0...1.11.0) (2024-11-19)


### Features

* kimi baseworker v3.6.0 ([fce90d9](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-pages/commit/fce90d95d55eedd17f0c5c95c72eef91ceb42ec9))
* store jpeg with 95% quality ([44f8ed5](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-pages/commit/44f8ed54c455602f61f734107573612823317005))

# [1.10.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-pages/compare/1.9.0...1.10.0) (2024-11-06)


### Features

* kimiworker 3.5.0 ([4dc7556](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-pages/commit/4dc7556fc9583a0bf1a5777516d68c7caf6a3a91))

# [1.9.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-pages/compare/1.8.0...1.9.0) (2024-11-06)


### Features

* kimiworker 3.3.1 ([931b4b7](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-pages/commit/931b4b7acb67a7eec5e231d9cf52bbbcdcec8fcf))

# [1.8.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-pages/compare/1.7.0...1.8.0) (2024-10-22)


### Features

* use kimiworker 2.15.0 with nldocspec 4.1.1 ([06c17c7](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-pages/commit/06c17c75172bc51522078a74e9e6556f2be952fe))

# [1.7.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-pages/compare/1.6.0...1.7.0) (2024-09-11)


### Features

* added volume mount for /tmp ([535f01a](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-pages/commit/535f01a48cdab9b468e03c2d316a241a309f53b4))

# [1.6.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-pages/compare/1.5.0...1.6.0) (2024-09-04)


### Features

* added securityContext to container and initContainers ([422e31c](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-pages/commit/422e31cbe8ee89acfbc4b64701b79b511a4684c4))

# [1.5.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-pages/compare/1.4.0...1.5.0) (2024-08-28)


### Features

* use new kimiworker to connect to station 4.0.0 and up ([3d6398c](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-pages/commit/3d6398c2c63f00f5219a9c7328162a4dc887532c))

# [1.4.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-pages/compare/1.3.0...1.4.0) (2024-07-04)


### Features

* support initContainers in helm values ([d9761c5](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-pages/commit/d9761c51d2068b9018e4d18795256f786e2d00d9))

# [1.3.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-pages/compare/1.2.0...1.3.0) (2024-06-05)


### Features

* support initContainers in helm values ([5782f3f](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-pages/commit/5782f3fd32d107e93100662ffaebce233738f48e))

# [1.2.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-pages/compare/1.1.3...1.2.0) (2024-06-04)


### Features

* correct worker name in chart ([4449e7b](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-pages/commit/4449e7bad6fa269cae5b0576b7e3bba086729cd2))

## [1.1.3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-pages/compare/1.1.2...1.1.3) (2024-05-13)


### Bug Fixes

* compatibility ([a8f6b89](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-pages/commit/a8f6b89b07015b9b356f113f70e4047e4787678e))
* update minio ([c24bb42](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-pages/commit/c24bb422e74d231e8b99ca0403fa5e87b1db6719))

## [1.1.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-pages/compare/1.1.1...1.1.2) (2024-04-24)


### Bug Fixes

* force release ([58e6b45](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-pages/commit/58e6b45836d9bc84ba296c25e12f78c14e33ea93))

## [1.1.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-pages/compare/v1.1.0...1.1.1) (2024-04-24)


### Bug Fixes

* force a release with the new pipeline ([6ffc3b0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-pages/commit/6ffc3b094f223a31600c4cdbc19e71e88905928b))

# [1.1.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-pages/compare/v1.0.0...v1.1.0) (2024-04-03)


### Bug Fixes

* on error, send success=False and confidence ([3eb4c48](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-pages/commit/3eb4c48e9d097392c73922019217859d0d3fe00f))


### Features

* set up tests, force first automated release ([2631e97](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-pages/commit/2631e97fc8c357b26e21f8ccdc45e1223e840762))
