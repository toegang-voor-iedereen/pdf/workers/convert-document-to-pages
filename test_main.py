import os, pytest
from unittest.mock import MagicMock, patch

from main import handler
from kimiworker import WorkerJob


@pytest.fixture
def mock_logger():
    return MagicMock()


@pytest.fixture
def mock_remote_storage():
    return MagicMock()


@pytest.fixture
def mock_publish():
    return MagicMock()


@pytest.fixture
def env_setup():
    with patch.dict(os.environ, {"BATCH_SIZE": "1", "THREAD_COUNT": "1"}):
        yield


def attribute_report(attributeName: str, stringValue: str):
    return {
        "attribute": attributeName,
        "bestJobId": "best-job-id",
        "expectedValues": 1,
        "isComplete": True,
        "publisher": {
            "station": {
                "id": "station-id",
                "type": "testingStation",
                "version": "0.1",
            },
            "endDate": "2024-07-04T00:12:34.000Z",
            "startDate": "2024-07-02T23:59:10.000Z",
        },
        "values": [
            {
                "confidence": 100,
                "jobId": "best-job-id",
                "recordId": "some-record-x",
                "resultType": "attributeWorkerResult",
                "stringResult": stringValue,
                "success": True,
                "timestamp": "2024-07-02T23:59:10.000Z",
                "traceId": "some-trace-id",
            },
        ],
    }


@pytest.fixture
def dependencies():
    log = MagicMock()
    job = MagicMock()
    job_id = "123"
    local_file_path = "/tmp/document.pdf"
    storage = MagicMock()
    publish = MagicMock()
    return log, job, job_id, local_file_path, storage, publish


def test_handler_missing_pagecount(dependencies, env_setup):
    log, job, job_id, local_file_path, storage, publish = dependencies
    job.get.side_effect = lambda key, default=None: {
        "recordId": "1",
        "traceId": "trace123",
        "attributes": {},
    }.get(key, default)

    handler(log, job, job_id, local_file_path, storage, publish)

    publish.assert_called_once_with("error", "PageCount not found")
    log.warning.assert_not_called()


def test_handler_zero_pagecount(dependencies, env_setup):
    log, job, job_id, local_file_path, storage, publish = dependencies
    job.get.side_effect = lambda key, default=None: {
        "recordId": "1",
        "traceId": "trace123",
        "attributes": {
            "pageCount": attribute_report("pageCount", "0"),
        },
    }.get(key, default)

    handler(log, job, job_id, local_file_path, storage, publish)

    publish.assert_called_with("error", "PageCount is zero (0)")
    log.warning.assert_called_once()


def test_no_page_count(mock_logger, mock_remote_storage, mock_publish):
    job: WorkerJob = {
        "bucketName": "bucket123",
        "filename": "test.pdf",
        "recordId": "record123",
        "attributes": {},
    }
    handler(mock_logger, job, "job123", "test.pdf", mock_remote_storage, mock_publish)
    mock_publish.assert_called_with("error", "PageCount not found")


def test_zero_page_count(mock_logger, mock_remote_storage, mock_publish):
    job: WorkerJob = {
        "bucketName": "bucket123",
        "filename": "test.pdf",
        "recordId": "record123",
        "attributes": {
            "pageCount": attribute_report("pageCount", "0"),
        },
    }

    handler(mock_logger, job, "job123", "test.pdf", mock_remote_storage, mock_publish)
    mock_publish.assert_called_with("error", "PageCount is zero (0)")


@patch("main.extract_pages")
def test_extraction_success(
    mock_extract, mock_logger, mock_remote_storage, mock_publish
):
    job: WorkerJob = {
        "bucketName": "bucket123",
        "filename": "test.pdf",
        "recordId": "record123",
        "attributes": {
            "pageCount": attribute_report("pageCount", "5"),
        },
    }

    mock_extract.return_value = {
        "success": True,
        "page_count": 5,
        "output_bucket_name": "pages",
        "output_bucket_path": "path/to/pages",
        "output_file_format": "jpg",
    }

    handler(mock_logger, job, "job123", "test.pdf", mock_remote_storage, mock_publish)

    mock_publish.assert_called_with(
        "pageResult",
        {
            "pageCount": 5,
            "bucketName": "pages",
            "bucketPath": "path/to/pages",
            "fileExtension": "jpg",
        },
        success=True,
        confidence=100,
    )


@patch("main.extract_pages")
def test_extraction_failure(
    mock_extract, mock_logger, mock_remote_storage, mock_publish
):
    job: WorkerJob = {
        "bucketName": "bucket123",
        "filename": "test.pdf",
        "recordId": "123",
        "attributes": {
            "pageCount": attribute_report("pageCount", "5"),
        },
    }

    mock_extract.return_value = {"success": False, "error": "Extraction failed"}

    handler(mock_logger, job, "job123", "test.pdf", mock_remote_storage, mock_publish)

    mock_publish.assert_called_with(
        "error", "Extraction failed", success=False, confidence=100
    )
