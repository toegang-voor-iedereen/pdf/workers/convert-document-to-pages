# syntax=docker/dockerfile:1

FROM python:3.13-slim

COPY requirements.txt requirements.txt

RUN apt-get update
RUN apt-get install poppler-utils -y
RUN pip3 install -r requirements.txt

COPY . .

CMD ["python3", "-u", "main.py"]