import time
import math
from typing import Literal, TypedDict
from kimiworker import KimiLogger, RemoteFileStorageInterface
from .extract_images import extract_images


class PageResultSuccess(TypedDict):
    success: bool
    page_count: int
    job_id: str
    output_bucket_name: str
    output_bucket_path: str
    output_file_format: str


class PageResultError(TypedDict):
    success: Literal[False]
    error: str


PageResult = PageResultSuccess | PageResultError


def extract_pages(
    storage: RemoteFileStorageInterface,
    log: KimiLogger,
    local_file_path: str,
    page_count: int,
    job_id: str,
    record_id: str,
    trace_id: str,
    batch_size: int,
    thread_count: int,
    output_bucket_name: str,
    output_file_format: str,
    output_content_type: str,
) -> PageResult:
    file_start_time = time.perf_counter()

    log.info(
        f"Handling job {job_id} with trace ID {trace_id}. Batch size: {batch_size}, thread count: {thread_count}"
    )

    if page_count == 0:
        log.error("Got pagecount of 0. Stopping processing for file.")
        return {"success": False, "error": "PageCount is zero"}

    log.info(f"Document Attributes report {page_count} pages")

    output_file_count = 0
    output_image_count = 0
    output_bucket_path = f"{record_id}/jobs/{job_id}"

    if page_count <= batch_size:
        log.info(f"Converting all images from file: {local_file_path}")

        batch_result = extract_images(
            storage=storage,
            log=log,
            file_path=local_file_path,
            output_bucket_name=output_bucket_name,
            output_bucket_path=output_bucket_path,
            output_file_format=output_file_format,
            output_content_type=output_content_type,
            start_page=None,
            end_page=None,
            thread_count=thread_count,
        )

        output_file_count = int(batch_result.get("output_file_count", 0))
        output_image_count = int(batch_result.get("output_image_count", 0))

    else:
        start_page = 0
        end_page = 0
        batch = 0
        batches = math.ceil(page_count / batch_size)

        log.info(f"Converting images in {batches} batches from file: {local_file_path}")
        while batch < batches:
            start_page = (batch * batch_size) + 1
            batch_end = start_page + batch_size - 1
            if batch_end > page_count:
                end_page = page_count
            else:
                end_page = batch_end

            batch_logger = log.child({"pageBatch": batch + 1})
            batch_logger.info(
                f"Batch {batch + 1}/{batches}, pages {start_page}-{end_page}/{page_count}"
            )

            batch_result = extract_images(
                storage=storage,
                log=batch_logger,
                file_path=local_file_path,
                output_bucket_name=output_bucket_name,
                output_bucket_path=output_bucket_path,
                output_file_format=output_file_format,
                output_content_type=output_content_type,
                start_page=start_page,
                end_page=end_page,
                thread_count=thread_count,
            )

            output_file_count += int(batch_result.get("output_file_count", 0))
            output_image_count += int(batch_result.get("output_image_count", 0))

            if batch_result.get("success") is False:
                error = batch_result.get("error")
                error = f"Batch failed: {error}"
                batch_logger.error(error)
                return {"success": False, "error": error}

            batch += 1

    file_end_time = time.perf_counter()
    file_time = file_end_time - file_start_time

    if output_image_count != page_count:
        error = f"Found descrepancy in output: Page count is {page_count} while number of images is {output_image_count}"
        log.error(error)
        return {"success": False, "error": error}

    if output_file_count != page_count:
        error = f"Found descrepancy in output: Page count is {page_count} while number of files is {output_file_count}"
        log.error(error)
        return {"success": False, "error": error}

    log.info(
        f"Finished file in {file_time} seconds: {page_count} pages, {output_file_count} files, {output_image_count} images"
    )
    return {
        "success": True,
        "page_count": page_count,
        "job_id": job_id,
        "output_bucket_name": output_bucket_name,
        "output_bucket_path": output_bucket_path,
        "output_file_format": output_file_format,
    }
