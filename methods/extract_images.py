import time
import tempfile
import os
import re
from typing import Literal, Optional, TypedDict
from pdf2image import convert_from_path
from kimiworker import KimiLogger, RemoteFileStorageInterface


class ImageResultSuccess(TypedDict):
    """Result of a succesfull image extraction"""

    success: bool
    output_image_count: int
    output_file_count: int


class ImageResultError(TypedDict):
    """Result of a failed image extraction"""

    success: Literal[False]
    error: str


ImageResult = ImageResultSuccess | ImageResultError


def extract_images(
    storage: RemoteFileStorageInterface,
    log: KimiLogger,
    file_path: str,
    output_bucket_name: str,
    output_bucket_path: str,
    output_file_format: str,
    output_content_type: str,
    start_page: Optional[int],
    end_page: Optional[int],
    thread_count: int,
) -> ImageResult:
    """Extract images from a PDF file"""
    batch_start_time = time.perf_counter()
    output_file_count = 0
    output_image_count = 0

    with tempfile.TemporaryDirectory() as batch_path:
        log.info(f"Output path: {batch_path}")
        jpeg_options = {
            "quality": 95,
            "progressive": "n",
            "optimize": "n",
        }

        if start_page is None:
            images = convert_from_path(
                file_path,
                output_folder=batch_path,
                thread_count=thread_count,
                fmt=output_file_format,
                output_file="",
                jpegopt=jpeg_options,
            )
        else:
            assert end_page is not None
            images = convert_from_path(
                file_path,
                output_folder=batch_path,
                first_page=start_page,
                last_page=end_page,
                thread_count=thread_count,
                fmt=output_file_format,
                output_file="",
                jpegopt=jpeg_options,
            )

        if images is None:
            log.error("Failed to convert bytes to images")
            return {"success": False, "error": "Failed to convert bytes to images"}

        output_image_count = len(images)
        log.info(f"Got {output_image_count} images.")

        file_list = os.listdir(batch_path)
        output_file_count = len(file_list)

        log.info(f"File list: {file_list}")

        page_number_regex = "^[0-9]{4}-([0-9]+)\\." + output_file_format + "$"
        for filename in file_list:
            found_page_number = re.findall(page_number_regex, filename)
            if len(found_page_number) != 1:
                log.error(f"Failed to find page number in filename: {filename}")
                return {"success": False, "error": "Regex error in filename"}

            page_number = int(found_page_number[0])
            log.info(
                f"Uploading page {page_number} of {filename} to Remote File Storage..."
            )
            storage.fput_object(
                bucket_name=output_bucket_name,
                object_name=f"{output_bucket_path}/{page_number}.{output_file_format}",
                file_path=f"{batch_path}/{filename}",
                content_type=output_content_type,
            )

    batch_end_time = time.perf_counter()
    log.info(f"Batch done in {batch_end_time - batch_start_time} seconds")
    return {
        "success": True,
        "output_image_count": output_image_count,
        "output_file_count": output_file_count,
    }
