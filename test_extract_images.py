import unittest, tempfile, os
from unittest.mock import Mock, patch, MagicMock, ANY
from methods.extract_images import extract_images


class TestExtractImages(unittest.TestCase):
    def setUp(self):
        self.storage_mock = Mock()
        self.logger_mock = Mock()
        self.temp_dir = tempfile.mkdtemp()
        self.test_pdf = os.path.join(self.temp_dir, "test.pdf")
        # Create an empty PDF file for testing
        with open(self.test_pdf, "wb") as f:
            f.write(b"%PDF-1.4")

        self.default_params = {
            "storage": self.storage_mock,
            "log": self.logger_mock,
            "file_path": self.test_pdf,
            "output_bucket_name": "test-bucket",
            "output_bucket_path": "test-path",
            "output_file_format": "jpg",
            "output_content_type": "image/jpeg",
            "start_page": None,
            "end_page": None,
            "thread_count": 1,
        }

    def tearDown(self):
        # Clean up temporary directory
        for root, dirs, files in os.walk(self.temp_dir, topdown=False):
            for name in files:
                os.remove(os.path.join(root, name))
            for name in dirs:
                os.rmdir(os.path.join(root, name))
        os.rmdir(self.temp_dir)

    @patch("methods.extract_images.convert_from_path")
    def test_successful_extraction_no_page_range(self, mock_convert):
        # Setup mock to return a list of two image objects
        mock_images = [MagicMock(), MagicMock()]
        mock_convert.return_value = mock_images

        # Setup mock filesystem
        with patch("os.listdir") as mock_listdir:
            mock_listdir.return_value = ["0001-1.jpg", "0001-2.jpg"]

            result = extract_images(**self.default_params)

        # Verify the result
        expected_result = {
            "success": True,
            "output_image_count": 2,
            "output_file_count": 2,
        }
        self.assertEqual(result, expected_result)

        # Verify convert_from_path was called with correct parameters
        mock_convert.assert_called_once_with(
            self.test_pdf,
            output_folder=ANY,  # Temporary directory will be different each time
            thread_count=1,
            fmt="jpg",
            output_file="",
            jpegopt={"quality": 95, "progressive": "n", "optimize": "n"},
        )

        # Verify storage calls
        self.assertEqual(self.storage_mock.fput_object.call_count, 2)

    @patch("methods.extract_images.convert_from_path")
    def test_successful_extraction_with_page_range(self, mock_convert):
        # Modify params to include page range
        params = self.default_params.copy()
        params["start_page"] = 1
        params["end_page"] = 2

        mock_images = [MagicMock(), MagicMock()]
        mock_convert.return_value = mock_images

        with patch("os.listdir") as mock_listdir:
            mock_listdir.return_value = ["0001-1.jpg", "0001-2.jpg"]

            result = extract_images(**params)

        expected_result = {
            "success": True,
            "output_image_count": 2,
            "output_file_count": 2,
        }
        self.assertEqual(result, expected_result)

        mock_convert.assert_called_once_with(
            self.test_pdf,
            output_folder=ANY,
            first_page=1,
            last_page=2,
            thread_count=1,
            fmt="jpg",
            output_file="",
            jpegopt={"quality": 95, "progressive": "n", "optimize": "n"},
        )

    @patch("methods.extract_images.convert_from_path")
    def test_failed_conversion(self, mock_convert):
        # Setup mock to return None, simulating conversion failure
        mock_convert.return_value = None

        result = extract_images(**self.default_params)

        expected_result = {
            "success": False,
            "error": "Failed to convert bytes to images",
        }
        self.assertEqual(result, expected_result)

    @patch("methods.extract_images.convert_from_path")
    def test_invalid_filename_pattern(self, mock_convert):
        # Setup mock to return a list of image objects
        mock_images = [MagicMock()]
        mock_convert.return_value = mock_images

        # Setup mock filesystem with invalid filename
        with patch("os.listdir") as mock_listdir:
            mock_listdir.return_value = ["invalid_filename.jpg"]

            result = extract_images(**self.default_params)

        expected_result = {"success": False, "error": "Regex error in filename"}
        self.assertEqual(result, expected_result)

    def test_type_annotations(self):
        # Verify the type hints are correct by checking if the function accepts
        # properly typed arguments
        from typing import get_type_hints

        hints = get_type_hints(extract_images)
        self.assertIn("storage", hints)
        self.assertIn("log", hints)
        self.assertIn("file_path", hints)
        self.assertIn("output_bucket_name", hints)
        self.assertIn("output_bucket_path", hints)
        self.assertIn("output_file_format", hints)
        self.assertIn("output_content_type", hints)
        self.assertIn("start_page", hints)
        self.assertIn("end_page", hints)
        self.assertIn("thread_count", hints)
        self.assertEqual(hints["start_page"].__name__, "Optional")
        self.assertEqual(hints["end_page"].__name__, "Optional")


if __name__ == "__main__":
    unittest.main()
