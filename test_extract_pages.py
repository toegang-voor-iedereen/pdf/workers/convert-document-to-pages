import unittest
from unittest.mock import Mock, patch, ANY
from methods.extract_pages import extract_pages


class TestExtractPages(unittest.TestCase):
    def setUp(self):
        self.storage_mock = Mock()
        self.logger_mock = Mock()
        self.logger_mock.child = Mock(return_value=Mock())

        # Default successful batch result
        self.success_batch_result = {
            "success": True,
            "output_file_count": 1,
            "output_image_count": 1,
        }

        self.default_params = {
            "storage": self.storage_mock,
            "log": self.logger_mock,
            "local_file_path": "/test/path/doc.pdf",
            "page_count": 1,
            "job_id": "test-job-123",
            "record_id": "test-record-456",
            "trace_id": "test-trace-789",
            "batch_size": 10,
            "thread_count": 2,
            "output_bucket_name": "test-bucket",
            "output_file_format": "jpg",
            "output_content_type": "image/jpeg",
        }

    @patch("methods.extract_pages.extract_images")
    def test_zero_page_count(self, mock_extract_images):
        params = self.default_params.copy()
        params["page_count"] = 0

        result = extract_pages(**params)

        expected_result = {"success": False, "error": "PageCount is zero"}
        self.assertEqual(result, expected_result)
        mock_extract_images.assert_not_called()

    @patch("methods.extract_pages.extract_images")
    def test_single_batch_success(self, mock_extract_images):
        mock_extract_images.return_value = self.success_batch_result

        result = extract_pages(**self.default_params)

        expected_result = {
            "success": True,
            "page_count": 1,
            "job_id": "test-job-123",
            "output_bucket_name": "test-bucket",
            "output_bucket_path": "test-record-456/jobs/test-job-123",
            "output_file_format": "jpg",
        }
        self.assertEqual(result, expected_result)

        mock_extract_images.assert_called_once_with(
            storage=self.storage_mock,
            log=self.logger_mock,
            file_path="/test/path/doc.pdf",
            output_bucket_name="test-bucket",
            output_bucket_path="test-record-456/jobs/test-job-123",
            output_file_format="jpg",
            output_content_type="image/jpeg",
            start_page=None,
            end_page=None,
            thread_count=2,
        )

    @patch("methods.extract_pages.extract_images")
    def test_multiple_batches_success(self, mock_extract_images):
        params = self.default_params.copy()
        params["thread_count"] = 1
        params["page_count"] = 20
        params["batch_size"] = 10

        mock_extract_images.return_value = {
            "success": True,
            "output_file_count": 10,
            "output_image_count": 10,
        }

        result = extract_pages(**params)

        print(result)

        self.assertTrue(result["success"], "should be successful")
        self.assertEqual(result["page_count"], 20, "should have a total of 20 pages")
        self.assertEqual(
            mock_extract_images.call_count,
            2,
            "should have been called twice, as batch size 10 for a total of 20 pages is 2 batches",
        )  # Ceil(25/10) = 3 batches

    @patch("methods.extract_pages.extract_images")
    def test_batch_failure(self, mock_extract_images):
        params = self.default_params.copy()
        params["page_count"] = 25
        params["batch_size"] = 10

        mock_extract_images.return_value = {
            "success": False,
            "error": "Conversion failed",
        }

        result = extract_pages(**params)

        expected_result = {"success": False, "error": "Batch failed: Conversion failed"}
        self.assertEqual(result, expected_result)
        self.assertEqual(
            mock_extract_images.call_count, 1
        )  # Should stop after first failure

    @patch("methods.extract_pages.extract_images")
    def test_image_count_mismatch(self, mock_extract_images):
        params = self.default_params.copy()
        params["page_count"] = 2

        mock_extract_images.return_value = {
            "success": True,
            "output_file_count": 2,
            "output_image_count": 1,  # Mismatch with page_count
        }

        result = extract_pages(**params)

        expected_result = {
            "success": False,
            "error": "Found descrepancy in output: Page count is 2 while number of images is 1",
        }
        self.assertEqual(result, expected_result)

    @patch("methods.extract_pages.extract_images")
    def test_file_count_mismatch(self, mock_extract_images):
        params = self.default_params.copy()
        params["page_count"] = 2

        mock_extract_images.return_value = {
            "success": True,
            "output_file_count": 1,  # Mismatch with page_count
            "output_image_count": 2,
        }

        result = extract_pages(**params)

        expected_result = {
            "success": False,
            "error": "Found descrepancy in output: Page count is 2 while number of files is 1",
        }
        self.assertEqual(result, expected_result)

    def test_type_annotations(self):
        from typing import get_type_hints

        hints = get_type_hints(extract_pages)
        required_params = [
            "storage",
            "log",
            "local_file_path",
            "page_count",
            "job_id",
            "record_id",
            "trace_id",
            "batch_size",
            "thread_count",
            "output_bucket_name",
            "output_file_format",
            "output_content_type",
        ]

        for param in required_params:
            self.assertIn(param, hints)

        self.assertEqual(hints["page_count"].__name__, "int")
        self.assertEqual(hints["batch_size"].__name__, "int")

    @patch("methods.extract_pages.extract_images")
    def test_last_batch_calculation(self, mock_extract_images):
        params = self.default_params.copy()
        params["page_count"] = 25
        params["batch_size"] = 10

        mock_extract_images.return_value = {
            "success": True,
            "output_file_count": 5,
            "output_image_count": 5,
        }

        extract_pages(**params)

        # Check the last batch had correct page range
        last_call_kwargs = mock_extract_images.call_args_list[-1][1]
        self.assertEqual(last_call_kwargs["start_page"], 21)
        self.assertEqual(last_call_kwargs["end_page"], 25)


if __name__ == "__main__":
    unittest.main()
