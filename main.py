#!/usr/bin/env python
import sys
import os
from kimiworker import (
    WorkerJob,
    PublishMethod,
    KimiLogger,
    Worker,
    RemoteFileStorageInterface,
    extract_best_attribute_value,
)
from methods.extract_pages import extract_pages


OUTPUT_BUCKET_NAME = "pages"
OUTPUT_FILE_FORMAT = "jpg"
OUTPUT_CONTENT_TYPE = "image/jpeg"

batch_size = int(os.getenv("BATCH_SIZE", "20"))
thread_count = int(os.getenv("THREAD_COUNT", "20"))


def handler(
    log: KimiLogger,
    job: WorkerJob,
    job_id: str,
    local_file_path: str,
    remote_file_storage: RemoteFileStorageInterface,
    publish: PublishMethod,
):
    """Handle extracting pages from documents"""
    record_id: str = job.get("recordId")
    trace_id: str = job.get("traceId", "")

    best_value = extract_best_attribute_value(job, "pageCount")

    if best_value is None:
        publish("error", "PageCount not found")
        return

    page_count: int = int(best_value["stringResult"])

    if page_count == 0:
        log.warning("Job cannot start: PageCount is zero (0)")
        publish("error", "PageCount is zero (0)")
        return

    job_result = extract_pages(
        storage=remote_file_storage,
        log=log,
        job_id=job_id,
        record_id=record_id,
        trace_id=trace_id,
        local_file_path=local_file_path,
        page_count=page_count,
        batch_size=batch_size,
        thread_count=thread_count,
        output_bucket_name=OUTPUT_BUCKET_NAME,
        output_file_format=OUTPUT_FILE_FORMAT,
        output_content_type=OUTPUT_CONTENT_TYPE,
    )

    if not job_result.get("success"):
        log.warning(f"Job unsuccesful: {job_result.get('error')}")
        publish(
            "error",
            job_result.get("error", "Unknown error"),
            success=False,
            confidence=100,
        )
        return

    log.info(f'Job succesful. Found {job_result.get("page_count")} pages.')

    page_result = {
        "pageCount": job_result.get("page_count", 0),
        "bucketName": job_result.get("output_bucket_name", ""),
        "bucketPath": job_result.get("output_bucket_path", ""),
        "fileExtension": job_result.get("output_file_format", ""),
    }

    publish("pageResult", page_result, success=True, confidence=100)


if __name__ == "__main__":
    try:  # pragma: no cover
        worker = Worker(handler, "worker-document-pages", True)
        worker.start()

    except KeyboardInterrupt:  # pragma: no cover
        print("\n")
        print("Got interruption signal. Exiting...\n")
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
